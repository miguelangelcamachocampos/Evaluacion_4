package utng.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_bebida")
public class Bebida implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id_bebida")
    private Long idBebida;
    @Column(name = "nombre_bebida", length = 100)
    private String nombreBebida;

    public Bebida(Long idBebida, String nombreBebida) {
        super();
        this.idBebida = idBebida;
        this.nombreBebida = nombreBebida;
    }

    public Bebida() {
        this.idBebida = 0L;
    }

    public Long getIdBebida() {
        return idBebida;
    }

    public void setIdBebida(Long idBebida) {
        this.idBebida = idBebida;
    }

    public String getNombreBebida() {
        return nombreBebida;
    }

    public void setNombreBebida(String nombreBebida) {
        this.nombreBebida = nombreBebida;
    }
}
