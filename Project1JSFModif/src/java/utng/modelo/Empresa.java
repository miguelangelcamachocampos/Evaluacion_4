package utng.modelo;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_empresa")
public class Empresa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_empresa")
    private Long idEmpresa;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "id_bebida")
    private String empresa;

    @Column(name = "cuidad", length = 100)
    private String cuidad;

    @Column(name = "precio", length = 100)
    private String precio;

    @Column(name = "cp")
    private int cp;

    @Column(name = "ruta", length = 100)
    private String ruta;

    public Empresa(Long idEmpresa, String empresa, String cuidad, String precio, int cp, String ruta) {
        super();
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.cuidad = cuidad;
        this.precio = precio;
        this.cp = cp;
        this.ruta = ruta;
    }

    public Empresa() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

 
    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCuidad() {
        return cuidad;
    }

    public void setCuidad(String cuidad) {
        this.cuidad = cuidad;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    
   
}
