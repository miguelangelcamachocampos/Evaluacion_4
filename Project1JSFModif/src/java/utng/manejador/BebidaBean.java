package utng.manejador;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import utng.datos.BebidaDAO;
import utng.modelo.Bebida;

@ManagedBean(name = "bebidaBean")
@SessionScoped
public class BebidaBean implements Serializable {

    private List<Bebida> bebidas;
    private Bebida bebida;

    public BebidaBean() {
    }

    public Bebida getBebida() {
        return bebida;
    }

    public void setBebida(Bebida bebida) {
        this.bebida = bebida;
    }

    public List<Bebida> getBebidas() {
        return bebidas;
    }

    public void setBebidas(List<Bebida> bebidas) {
        this.bebidas = bebidas;
    }

    public String listar() {
        BebidaDAO dao = new BebidaDAO();
        try {
            bebidas = dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Bebidas";
    }

    public String eliminar() {
        BebidaDAO dao = new BebidaDAO();
        try {
            dao.delete(bebida);
            bebidas = dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Eliminar";
    }

    public String iniciar() {
        bebida = new Bebida();
        return "Iniciar";
    }

    public String guardar() {
        BebidaDAO dao = new BebidaDAO();
        try {
            if (bebida.getIdBebida() != 0) {
                dao.update(bebida);
            } else {
                dao.insert(bebida);
            }
            bebidas = dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Guardar";
    }

    public String cancelar() {
        return "Cancelar";
    }

    public String editar(Bebida bebida) {
        this.bebida = bebida;
        return "Editar";
    }
}
