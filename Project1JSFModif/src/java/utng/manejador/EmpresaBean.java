package utng.manejador;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import utng.datos.EmpresaDAO;
import utng.datos.BebidaDAO;
import utng.modelo.Empresa;
import utng.modelo.Bebida;

@ManagedBean(name = "empresaBean")
@SessionScoped
public class EmpresaBean implements Serializable {

    private List<Empresa> empresas;
    private Empresa empresa;
    private Bebida bebida;
    private List<Bebida> bebidas;

    public EmpresaBean() {
        empresa = new Empresa();
        empresa = new Empresa();
        //empresa.setCuidad(new Empresa());
    }

    public List<Bebida> getBebidas() {
        return bebidas;
    }

    public void setBebidas(List<Bebida> bebidas) {
        this.bebida = (Bebida) bebidas;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }

    public String listar() {
        EmpresaDAO dao = new EmpresaDAO();
        try {
            empresas = dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Calificaciones";
    }

    public String eliminar() {
        EmpresaDAO dao = new EmpresaDAO();
        try {
            dao.delete(empresa);
            empresas = dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Eliminar";
    }

    public String iniciar() {
        empresa = new Empresa();
       // empresa.setCuidad(new Empresa());
        try {
            bebidas = new BebidaDAO().getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Iniciar";
    }

    public String guardar() {
        EmpresaDAO dao = new EmpresaDAO();
        try {
            if (empresa.getIdEmpresa() != 0) {
                dao.update(empresa);
            } else {
                dao.insert(empresa);
            }
            empresas = dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Guardar";
    }

    public String cancelar() {
        return "Cancelar";
    }

    public String editar(Empresa empresa) {
        this.empresa = empresa;
        try {
            bebidas = new BebidaDAO().getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Editar";
    }

}
