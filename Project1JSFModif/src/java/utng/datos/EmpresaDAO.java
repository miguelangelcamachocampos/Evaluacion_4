
package utng.datos;

import org.hibernate.HibernateException;
import utng.modelo.Empresa;

public class EmpresaDAO extends DAO<Empresa>{
    
    public EmpresaDAO() {
        super(new Empresa());
    }
    
    public Empresa getOneById(Empresa empresa)
            throws HibernateException {
        return super.getOneById(empresa.getIdEmpresa());
    }
}
