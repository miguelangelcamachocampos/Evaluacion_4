package utng.datos;

import org.hibernate.HibernateException;
import utng.modelo.Bebida;

public class BebidaDAO extends DAO<Bebida>{
    //Constructor
    public BebidaDAO() {
        super(new Bebida());
    }

    public Bebida getOneById(Bebida bebida)
            throws HibernateException {
        return super.getOneById(bebida.getIdBebida());
    }    
}
